package gui;

import generation.CardinalDirection;
import generation.Distance;
import generation.MazeConfiguration;
import gui.Robot.Turn;

/**
 * Wizard knows the distance matrix and uses it to find the exit.
 * @author Yiru Zhang
 *
 */
public class Wizard implements RobotDriver {

	// private variables
	private Robot bot;
	private int width;
	private int height;
	private Distance dist;
	private MazeConfiguration config;
	
	@Override
	public void setRobot(Robot r) {
		// TODO Auto-generated method stub
		if (r == null) {
			System.out.println("Wizard: setRobot() should not set a null bot");
			return;
		}
		this.bot = r;
		//config = ((BasicRobot)r).getConfig();
	}

	@Override
	public void setDimensions(int width, int height) {
		// TODO Auto-generated method stub
		this.width = width;
		this.height = height;
	}

	@Override
	public void setDistance(Distance distance) {
		// TODO Auto-generated method stub
		this.dist = distance;
	}

	/**
	 * Use the distance matrix to find the exit. 
	 * Check which neighboring cell gets closer to the exit 
	 * and this is the cell to go next.
	 * From P3 mannual: wizard class is intended to work as the
	 * baseline algorithm to see if most efficient energy consumption
	 * and path length as well for testing purposes.
	 * 
	 * Note: the directions in the distance matrix is TOTALLY 
	 * different from the notation that is normally used in the 
	 * robot. In specific, the positive x direction in the dist is 
	 * the negative y direction in robot; the positive y direction
	 * in dist is the positive x direction in robot.
	 */
	@Override
	public boolean drive2Exit() throws Exception {
		// TODO Auto-generated method stub
//		printDist();
		int cx, cy, nx, ny;
		// (cx, cy) is the current position
		// (nx, ny) is the next position
		config = ((BasicRobot)bot).getConfig();
		System.out.println("Wizard: start drive2Exit()");
		// for debug
		if (bot == null) {
			System.out.println("Wizard: d2e() bot == null, throw");
			throw new Exception();
		}
		if (config == null) {
			System.out.println("Wizard: d2e() config == null, throw");
			throw new Exception();
		}
		
		int debug = 0;
		while (!bot.isAtExit()) {
			debug++;
			cx = bot.getCurrentPosition()[0];
			cy = bot.getCurrentPosition()[1];
			nx = config.getNeighborCloserToExit(cx, cy)[0];
			ny = config.getNeighborCloserToExit(cx, cy)[1];
			
			// if bot runs out of battery, throw exception
			if (bot.getBatteryLevel() <= 0) {
				System.out.println("Wizard: bot stops because it runs out of battery!");
				throw new Exception();
			}
						
//			System.out.println("Wizard: inside d2e() while loop (a new cycle)");
//			System.out.println("current position: (" + cx + ", " + cy + ")");
//			System.out.println("next position: (" + nx + ", " + ny + ")");
//			System.out.println("cur direction: " + bot.getCurrentDirection());
//			System.out.println("dist is: ");
//			printDist(config.getMazedists());
//			System.out.println("curPos dist is: " + config.getDistanceToExit(cx, cy));
//			System.out.println("nxtPos dist is: " + config.getDistanceToExit(nx, ny));

			// north / south
			if (cx == nx) {
				if (cy - ny == -1) {	//rotate to south and move 1 step
					turnTo(CardinalDirection.South);
					bot.move(1, false);
				}
				if (cy - ny == 1) {		//rotate to north and move 1 step
					turnTo(CardinalDirection.North);
					bot.move(1, false);
				}
			}
			
			// east / west
			if (cy == ny) {
				if (cx - nx == -1) {	//rotate to east and move 1 step
					turnTo(CardinalDirection.East);
					bot.move(1, false);
				}
				if (cx - nx == 1) {		//rotate to west and move 1 step
					turnTo(CardinalDirection.West);
					bot.move(1, false);
				}
			}
			// for debug
//			if (debug > 200) {
//				System.out.println("INFINITE LOOP, debug = " + debug);
//				throw new Exception();
//			}
		}
		// bot finds the exit, returns true
		return true;
	}

	@Override
	public float getEnergyConsumption() {
		// TODO Auto-generated method stub
		return 2000f - this.bot.getBatteryLevel();
	}

	@Override
	public int getPathLength() {
		// TODO Auto-generated method stub
		return this.bot.getOdometerReading();
	}
	
	/////////////////////private methods/////////////////////////
	private void turnTo(CardinalDirection cd) {
		switch (cd) {
		case North:
			if (bot.getCurrentDirection() == CardinalDirection.East)
				bot.rotate(Turn.RIGHT);
			if (bot.getCurrentDirection() == CardinalDirection.West)
				bot.rotate(Turn.LEFT);
			if (bot.getCurrentDirection() == CardinalDirection.South)
				bot.rotate(Turn.AROUND);
			break;
		case South:
			if (bot.getCurrentDirection() == CardinalDirection.East)
				bot.rotate(Turn.LEFT);
			if (bot.getCurrentDirection() == CardinalDirection.West)
				bot.rotate(Turn.RIGHT);
			if (bot.getCurrentDirection() == CardinalDirection.North)
				bot.rotate(Turn.AROUND);
			break;
		case West:
			if (bot.getCurrentDirection() == CardinalDirection.East)
				bot.rotate(Turn.AROUND);
			if (bot.getCurrentDirection() == CardinalDirection.North)
				bot.rotate(Turn.RIGHT);
			if (bot.getCurrentDirection() == CardinalDirection.South)
				bot.rotate(Turn.LEFT);
			break;
		case East:
			if (bot.getCurrentDirection() == CardinalDirection.West)
				bot.rotate(Turn.AROUND);
			if (bot.getCurrentDirection() == CardinalDirection.North)
				bot.rotate(Turn.LEFT);
			if (bot.getCurrentDirection() == CardinalDirection.South)
				bot.rotate(Turn.RIGHT);
			break;
		}
	}
	
	
	///////////////////////////////private methods////////////////
	protected int getWidth() {
		return this.width;
	}
	
	protected int getHeight() {
		return this.height;
	}
	
	protected Distance getDist() {
		return this.dist;
	}
	
	// debug
//	private void printDist(Distance dist) {
//		if (dist == null) {
//			System.out.println("dist is null, return");
//			return;
//		}
//		for (int i =  dist.getAllDistanceValues().length - 1; i >= 0; --i) {
//			for (int j = 0; j < dist.getAllDistanceValues()[0].length; ++j) {
//				System.out.print(dist.getDistanceValue(j, i) + " ");
//			}
//			System.out.println();
//		}
//	}
	
	

}
