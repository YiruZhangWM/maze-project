package gui;

import generation.CardinalDirection;
import generation.Cells;
import generation.Distance;
import generation.MazeConfiguration;
import gui.Constants.UserInput;

/**
 * As an implementation of Robot class, BasicRobot stores
 * the information of 
 * @author Yiru Zhang
 *
 */
public class BasicRobot implements Robot {

	// private variables
	private Controller control;
	private MazeConfiguration config;
	private boolean hasStop = false;
	private Distance dist;
	private Cells cell;
	private int[] curPos;
	private CardinalDirection curCardinalDir;
	private boolean hasRmSensor;
	private boolean[] hasDistSensor;
	private int odoReading;
	private float batteryLevel;
	//private Direction curSelfDir;
	
	public BasicRobot() {
		this.hasDistSensor = new boolean[] {true, true, true, true};
		// hasDistSensor indicates if distance sensor is 
		// available at [LEFT, FORWARD, RIGHT, BACKWARD]
		this.hasRmSensor = true;
		// default to have room sensor
	}
	
	public void init() {
		this.config = control.getMazeConfiguration();

		if (this.config == null) {
			System.out.println("init: config cannot be null");
			return;
		}
//		System.out.println("init is normal: config is NOT null");
		setStuff();
		this.batteryLevel = 2000f;
		
//		System.out.println("from init() in BasicRobot");
//		System.out.println(this.control.getCurrentPosition()[0]);
	}
	
	/**
	 * Turn robot on the spot for amount of degrees.
	 * If robot runs out of energy, it stops.
	 * ord stores the current index of the enum Turn
	 * and is responsible for the record of the 
	 * current Cardinal Direction (absolute direction).
	 */
	@Override
	public void rotate(Turn turn) {
		// debug
		//System.out.println("Before rotation, curCardinalDirection is " + this.curCardinalDir);
				
		if (this.hasStop) {
			System.out.println("BasicRobot has stopped, cannot proceed to rotate");
			return;
		}
		
		// handles the direction
		// int ord = this.curCardinalDir.ordinal();
		
		// try to use controller
		switch(turn) {
		case LEFT:
			this.batteryLevel -= 3.0;
			control.keyDown(UserInput.Left, 0);	//? why inverted
			break;
		case RIGHT:
			this.batteryLevel -= 3.0;
			control.keyDown(UserInput.Right, 0);
			break;
		case AROUND:
			this.batteryLevel -= 6.0;
			control.keyDown(UserInput.Left, 0);
			control.keyDown(UserInput.Left, 0);
			break;
		}
		setStuff();
		
//		switch(turn) {
//		case LEFT:
//			this.batteryLevel -= 3.0;
//			ord = (ord + 3) % 4;
//			break;
//		case RIGHT:
//			this.batteryLevel -= 3.0;
//			ord = (ord + 1) % 4;
//			break;
//		case AROUND:
//			this.batteryLevel -= 6.0;
//			ord = (ord + 2) % 4;
//			break;
//		}
		System.out.println("BasicRobot: rotate() battery is: " + this.batteryLevel);
		if (this.batteryLevel <= 0) {
			System.out.println("STOP FOR INSUFFICIENT BATTERY, battery = 0");
			this.batteryLevel = 0;
			hasStop = true;
			return;
		}
//		System.out.println("cd was: " + this.curCardinalDir);
//		this.curCardinalDir = CardinalDirection.values()[ord];
//		System.out.println("cd is now: " + this.curCardinalDir);
		// debug
//		System.out.println("After rotation, curCardinalDirection is " + this.curCardinalDir);
	}

	@Override
	public void move(int distance, boolean manual) {		
		// TODO Auto-generated method stub
		System.out.println("BasicRobot: move()");
		for (int i = 0; i < distance; ++i) {
			if (this.hasStop) {
				System.out.println("BasicRobot has stopped, cannot proceed to move");
				return;
			}
			
			// change battery level
			this.batteryLevel -= 4.0;
			System.out.println("BasicRobot: move() battery level is: " + this.batteryLevel);
			if (this.batteryLevel < 0) {
				this.batteryLevel = 0;
				hasStop = true;
				return;
			}
			
			// if wall is hit
			if (config.hasWall(curPos[0], curPos[1], curCardinalDir)) {
				if (!manual) {
					System.out.println("Something goes wrong: an algorithm used and wall hitted");
					this.hasStop = true;
				}
				System.out.println("You hit a wall");
				return;
			}
			
			int x, y;
			x = this.curPos[0];
			y = this.curPos[1];
			// move one step
//			System.out.println("FROM BasicRobot: ");
//			System.out.println("before move(), (x, y) is: (" + x + ", " + y + ")");
			control.keyDown(UserInput.Up, 0);
			setStuff();
			x = this.curPos[0];
			y = this.curPos[1];
//			System.out.println("after move(), (x, y) is: (" + x + ", " + y + ")");
			this.odoReading += 1;
		}
	}

	@Override
	public int[] getCurrentPosition() throws Exception {
		// TODO Auto-generated method stub
		return this.curPos;
	}

	@Override
	public void setMaze(Controller controller) {
		// TODO Auto-generated method stub
		this.control = controller;
		
//		int x = controller.getCurrentPosition()[0];
//		System.out.println("controller parameter setMaze() in BasicRobot");
//		System.out.println("setMaze(): x should be 0 after setting:" + x);
//		
//		x = this.control.getCurrentPosition()[0];
//		System.out.println("this.control setMaze() in BasicRobot");
//		System.out.println("setMaze(): x should be 0 after setting:" + x);
		
	}

	@Override
	public boolean isAtExit() throws Exception {
		// TODO Auto-generated method stub
		return dist.isExitPosition(curPos[0], curPos[1]);
	}

	@Override
	public boolean canSeeExit(Direction direction) throws UnsupportedOperationException {
		// TODO Auto-generated method stub
		// if the current position is exit, return true
		if (dist.isExitPosition(curPos[0], curPos[1])) {
			return true;
		}
		
		CardinalDirection cd = getCDforGivenDirection(direction);
		int x = this.curPos[0];
		int y = this.curPos[1];
		boolean seeExit = false;

		while (!config.hasWall(x, y, cd)) {
			switch (cd) {
			case North:
				y -= 1;
				break;
			case South:
				y += 1;
				break;
			case West:
				x -= 1;
				break;
			case East:
				x += 1;
				break;
			}
			
			seeExit = dist.isExitPosition(x, y);
			if (seeExit) {
				break;
			}
			
			//System.out.println("CANSEEEXIT: seeExit is: " + seeExit);
		}
		this.batteryLevel -= 1;
		return seeExit;
	}

	@Override
	public boolean isInsideRoom() throws UnsupportedOperationException {
		// TODO Auto-generated method stub
		return this.cell.isInRoom(curPos[0], curPos[1]);
	}

	@Override
	public boolean hasRoomSensor() {
		// TODO Auto-generated method stub
		return this.hasRmSensor;
	}

	@Override
	public CardinalDirection getCurrentDirection() {
		// TODO Auto-generated method stub
		return this.curCardinalDir;
	}

	@Override
	public float getBatteryLevel() {
		// TODO Auto-generated method stub
		return this.batteryLevel;
	}

	@Override
	public void setBatteryLevel(float level) {
		// TODO Auto-generated method stub
		this.batteryLevel = level;
	}

	@Override
	public int getOdometerReading() {
		// TODO Auto-generated method stub
		return this.odoReading;
	}

	@Override
	public void resetOdometer() {
		// TODO Auto-generated method stub
		this.odoReading = 0;
	}

	@Override
	public float getEnergyForFullRotation() {
		// TODO Auto-generated method stub
		return 12.0f;
	}

	@Override
	public float getEnergyForStepForward() {
		// TODO Auto-generated method stub
		return 4.0f;
	}

	@Override
	public boolean hasStopped() {
		// TODO Auto-generated method stub
		return this.hasStop;
	}

	/**
	 * tells the dist to an obstacle (wall or border)
	 * in the given direction
	 */
	@Override
	public int distanceToObstacle(Direction direction) throws UnsupportedOperationException {
		// TODO Auto-generated method stub
		int count = 0;		// count the number of steps free
							// of obstacles
		
		// if the robot does not have sensor at
		// the given direction, throw exception
		if (!this.hasDistSensor[direction.ordinal()]) {
			throw new UnsupportedOperationException();
		}
		
		int x = this.curPos[0];
		int y = this.curPos[1];
		System.out.println("BasicRobot: current position is (" + x + ", " + y + ")");
		// get the direction of the distance sensor
		CardinalDirection cd = getCDforGivenDirection(direction);
		
		System.out.println(cd + " direction has wall is: " + config.hasWall(x, y, cd));
		while (!config.hasWall(x, y, cd)) {
			count += 1;
						
			//System.out.println("prev d2o: (x, y): (" + x + ", " + y + ")");
			switch (cd) {
			case North:
				y -= 1;
				break;
			case South:
				y += 1;
				break;
			case West:
				x -= 1;
				break;
			case East:
				x += 1;
				break;
			}
//			System.out.println("aft d2o: (x, y): (" + x + ", " + y + ")");
//			System.out.println("count is: " + count);

			if (x < 0 || x > config.getWidth() - 1 ||
				y < 0 || y > config.getHeight() - 1) {
				System.out.println("BasicRobot: dist2Obs problem!");
				assert false;
			}
		}
		this.batteryLevel -= 1;
		// if the battery level is already 0, set it to 0 to avoid 
		// negative battery level
		if (this.batteryLevel <= 0) {
			this.batteryLevel = 0;
		}
		return count;
	}

	/**
	 * the boolean[] hadDistSensor keeps record of the info
	 * with the corresponding order: [LEFT, FWD, RIGHT, BKD]
	 */
	@Override
	public boolean hasDistanceSensor(Direction direction) {
		// TODO Auto-generated method stub
		return this.hasDistSensor[direction.ordinal()];
	}
	
	/**
	 * fetch the MazeConfiguration for Wizard driver
	 * @return the robot's maze configuration
	 */
	public MazeConfiguration getConfig() {
		return this.config;
	}
	
	//////////////////////////private methods////////////////////
	/**
	 * For reference: 
	 * enum CardinalDirection {N, E, S, W}
	 * @param direction
	 * @return the cardinal direction for a given direction
	 */
	private CardinalDirection getCDforGivenDirection(Direction direction) {
		int ord = this.curCardinalDir.ordinal();
		switch (direction) {
		case FORWARD:
			break;
		case BACKWARD:
			ord = (ord + 2) % 4;
			break;
		case LEFT:
			ord = (ord + 1) % 4;
			break;
		case RIGHT:
			ord = (ord + 3) % 4;
			break;
		}
		return CardinalDirection.values()[ord];
	}

	private void setStuff() {
		this.dist = config.getMazedists();
		this.cell = config.getMazecells();
		this.curPos = control.getCurrentPosition();
		this.curCardinalDir = control.getCurrentDirection();
	}
}
