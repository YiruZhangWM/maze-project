/**
 * 
 */
package gui;

import generation.Order;

import java.awt.event.KeyListener;
import java.io.File;

import javax.swing.JFrame;


/**
 * This class is a wrapper class to startup the Maze game as a Java application
 * 
 * This code is refactored code from Maze.java by Paul Falstad, www.falstad.com, Copyright (C) 1998, all rights reserved
 * Paul Falstad granted permission to modify and use code for teaching purposes.
 * Refactored by Peter Kemper
 * 
 * TODO: use logger for output instead of Sys.out
 */
public class MazeApplication extends JFrame {

	// not used, just to make the compiler, static code checker happy
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 */
	public MazeApplication() {
		init(null, null, null);
	}

	/**
	 * Constructor that loads a maze from a given file or uses a particular method to generate a maze
	 * @param parameter can identify a generation method (Prim, Kruskal, Eller)
     * or a filename that stores an already generated maze that is then loaded, or can be null
	 */
	public MazeApplication(String builderParameter, String driverParameter, String filename) {
		init(builderParameter, driverParameter, filename);
	}

	BasicRobot bot;
	RobotDriver driver;
	
	/**
	 * Instantiates a controller with settings according to the given parameter.
	 * @param parameter can identify a generation method (Prim, Kruskal, Eller)
	 * or a filename that contains a generated maze that is then loaded,
	 * or can be null
	 * @return the newly instantiated and configured controller
	 */
	 Controller createController(String builderParameter, String driverParameter, String filename) {
	    // need to instantiate a controller to return as a result in any case
	    Controller result = new Controller() ;
	    String msg = null, dMsg = null, fMsg = null; // message for feedback
	    
	    // for filename parameter
	    if (filename != null) {
	        File f = new File(filename) ;
	        if (f.exists() && f.canRead())
	        {
	            fMsg = "MazeApplication: loading maze from file: " + filename;
	            result.setFileName(filename);
	            //return result;
	        }
	        else {
	            // None of the predefined strings and not a filename either: 
	            fMsg = "MazeApplication: unknown parameter value: " + filename + " ignored, operating in default mode.";
	        }
	    }
	    
	    // for builder parameter--applies only if filename is null
	    if (filename == null) {
	    	// Case 1: no input
	    	if (builderParameter == null) {
	    		msg = "MazeApplication: maze will be generated with a randomized algorithm."; 
	    	}
	    	// Case 2: Prim
	    	else if ("Prim".equalsIgnoreCase(builderParameter))
	    	{
	    		msg = "MazeApplication: generating random maze with Prim's algorithm.";
	    		result.setBuilder(Order.Builder.Prim);
	    	}
	    	// Case 3 a and b: Eller, Kruskal or some other generation algorithm
	    	else if ("Kruskal".equalsIgnoreCase(builderParameter))
	    	{
	    		// TODO: for P2 assignment, please add code to set the builder accordingly
	    		msg = "MazeApplication: generating random maze with Kruskal's algorithm.";
	    		result.setBuilder(Order.Builder.Kruskal);
	    	}
	    	else if ("Eller".equalsIgnoreCase(builderParameter))
	    	{
	    		// TODO: for P2 assignment, please add code to set the builder accordingly
	    		throw new RuntimeException("Don't know anybody named Eller ...");
	    	}
	    }
	    
	    bot = new BasicRobot();
	    //RobotDriver driver;
	    
	    // for driver parameter
	    if (driverParameter == null) {
	    	dMsg = "MazeApplication: maze will be drived manually.";
	    }
	    if ("Wallfollower".equalsIgnoreCase(driverParameter)) {
	    	dMsg = "MazeApplication: maze will be drived by WallFollower algorithm.";
	    	driver = new WallFollower();
	    	driver.setRobot(bot);
	    	result.setRobotAndDriver(bot, driver);
	    	bot.setMaze(result);
	    }
	    if ("Wizard".equalsIgnoreCase(driverParameter)) {
	    	dMsg = "MazeApplication: maze will be drived by Wizard algorithm.";
	    	driver = new Wizard();
	    	driver.setRobot(bot);
	    	result.setRobotAndDriver(bot, driver);
	    	bot.setMaze(result);
	    }
	    
	    // controller instanted and attributes set according to given input parameter
	    // output message and return controller
//	    System.out.println("mazeApp.createController:");
//	    System.out.println("bmsg: " + msg);
//	    System.out.println("dmsg: " + dMsg);
//	    System.out.println("fmsg: " + fMsg);
	    return result;
	}

	/**
	 * Initializes some internals and puts the game on display.
	 * @param parameter can identify a generation method (Prim, Kruskal, Eller)
     * or a filename that contains a generated maze that is then loaded, or can be null
	 */
	private void init(String builderParameter, String driverParameter, String filename) {
	    // instantiate a game controller and add it to the JFrame
	    Controller controller = createController(builderParameter, driverParameter, filename);
		add(controller.getPanel()) ;
		// instantiate a key listener that feeds keyboard input into the controller
		// and add it to the JFrame
		KeyListener kl = new SimpleKeyListener(this, controller) ;
		addKeyListener(kl) ;
		// set the frame to a fixed size for its width and height and put it on display
		setSize(400, 400) ;
		setVisible(true) ;
		// focus should be on the JFrame of the MazeApplication and not on the maze panel
		// such that the SimpleKeyListener kl is used
		setFocusable(true) ;
		// start the game, hand over control to the game controller
		controller.start();
	}
	
	/**
	 * Main method to launch Maze game as a java application.
	 * The application can be operated in three ways. 
	 * 1) The intended normal operation is to provide no parameters
	 * and the maze will be generated by a randomized DFS algorithm (default). 
	 * 2) If a filename is given that contains a maze stored in xml format. 
	 * The maze will be loaded from that file. 
	 * This option is useful during development to test with a particular maze.
	 * 3) A predefined constant string is given to select a maze
	 * generation algorithm, currently supported is "Prim".
	 * @param args is optional, first string can be a fixed constant like Prim or
	 * the name of a file that stores a maze in XML format
	 */
	public static void main(String[] args) {
	    JFrame app ; 
	    int i = 0, loopCounter = 0;
	    String curArg;
	    String bArg = null, dArg = null, file = null;
	    
	    if (args.length == 0) {
	    	app = new MazeApplication();
	    }
	    
	    while (i < args.length && args[i].startsWith("-")) {
	    	curArg = args[i++];
	    	loopCounter++;
	    	
	    	// -f to select a file to load a maze from
	    	if (curArg.equals("-f")) {
	    		if (i >= args.length) {
	    			System.out.println("more arguments needed");
	    			System.exit(0);
	    		}
	    		
	    		curArg = args[i++];
	    		file = curArg;
	    	}
	    	
	    	// -g to select a generation algorithm
	    	if (curArg.equals("-g")) {
	    		if (i >= args.length) {
	    			System.out.println("more arguments needed");
	    			System.exit(0);
	    		}
	    		curArg = args[i++];
	    		
	    		if (curArg.equalsIgnoreCase("Prim")) {
	    			bArg = "Prim";
	    		}
	    		if (curArg.equalsIgnoreCase("Kruskal")) {
	    			bArg = "Kruskal";
	    		}
	    		if (!curArg.equalsIgnoreCase("Prim") &&
	    			!curArg.equalsIgnoreCase("Kruskal") ) {
	    			System.out.println("illegal -g argument: [" + curArg + "]");
	    			System.exit(0);
	    		}
	    	}
	    	
	    	// -d to select a driver algorithm
	    	if (curArg.equals("-d")) {
	    		if (i >= args.length) {
	    			System.out.println("more arguments needed");
	    			System.exit(0);
	    		}
	    		curArg = args[i++];
	    		
	    		if (curArg.equalsIgnoreCase("Wizard")) {
	    			dArg = "Wizard";
	    		}
	    		
	    		if (curArg.equalsIgnoreCase("Wallfollower")) {
	    			dArg = "Wallfollower";
	    		}
	    		if (!curArg.equalsIgnoreCase("Wizard") &&
	    			!curArg.equalsIgnoreCase("Wallfollower")) {
	    			System.out.println("illegal -d argument");
	    			System.exit(0);
	    		}
	    	}
	    	
	    	// handle exceptions
	    	if (loopCounter > 100) {
	    		System.out.println("invalid dash statement");
	    		System.exit(0);
	    	}
	    }
	    
//	    System.out.println("FROM MAIN");
//	    System.out.println("bArg: " + bArg);
//	    System.out.println("dArg: " + dArg);
//	    System.out.println("file: " + file);
	    
	    app = new MazeApplication(bArg, dArg, file);
	    
//		switch (args.length) {
//		case 1 : app = new MazeApplication(args[0]);
//		break ;
//		case 0 : 
//		default : app = new MazeApplication() ;
//		break ;
//		}
		app.repaint() ;
	}

}
