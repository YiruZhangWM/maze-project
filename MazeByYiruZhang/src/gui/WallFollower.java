package gui;

import generation.Distance;
import gui.Robot;
import gui.Robot.Direction;
import gui.Robot.Turn;

/**
 * WallFollower implements RobotDriver and provides a brute force 
 * approach to solve the maze.
 * @author Yiru Zhang
 *
 */
public class WallFollower implements RobotDriver {

	// private variables
	private Robot bot;
	private int width;
	private int height;
	private Distance dist;

//	/**
//	 * energyConsumed keeps record of the total amount of 
//	 * energy consumed by the robot driver;
//	 * in this case, it is initialized as 0.
//	 */
//	private float energyConsumed = 0f;
//	/**
//	 * pathLen keeps record of the total number of 
//	 * path length traveled by the robot driver;
//	 * in this case, it is initialized as 0.
//	 */
//	private int pathLen = 0;
	
	@Override
	public void setRobot(Robot r) {
		// TODO Auto-generated method stub
//		if (r == null) {
//			assert false: "WallFollower: robot cannot be set to null";
//		}
		this.bot = r;
	}

	@Override
	public void setDimensions(int width, int height) {
		// TODO Auto-generated method stub
		this.width = width;
		this.height = height;
	}

	@Override
	public void setDistance(Distance distance) {
		// TODO Auto-generated method stub
//		if (distance == null) {
//			assert false: "WallFollower: distance cannot be set to null";
//		}
		this.dist = distance;
	}

	/**
	 * The intuition of the algorithm is as followed:
	 * 1. start following passages;
	 * 2. when reach a junction, always turn LEFT;
	 * 3. when there are both a left and front wall, turn RIGHT;
	 * 4. loop termination conditions: a) bot reaches the 
	 * exit, win; b) bot runs out of battery, lose.
	 */
	@Override
	public boolean drive2Exit() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("WallFollower: start drive2exit()");
		int dbg = 0;
		for ( ; ; ) {
			System.out.println("WallFollower: start for loop");
			System.out.println("WallFollower: battery level is: " + bot.getBatteryLevel());
			dbg++;
			// if bot is at exit, return
			if (bot.isAtExit()) {
				System.out.println("Exit is reached! return true");
				return true;
			}
			// if bot can see the exit, move forward
			if (bot.canSeeExit(Direction.FORWARD)) {
				System.out.println("Exit is in front!");
				bot.move(1, false);
				continue;
			}
			// if bot runs out of battery, throw exception
			if (bot.getBatteryLevel() <= 0) {
				System.out.println("WallFollower: bot stops because it runs out of battery!");
				throw new Exception();
			}
			
			// if there is a junction, turn left and step forward
			if (bot.distanceToObstacle(Direction.LEFT) != 0) {
				System.out.println("WallFollower: turn left");
				bot.rotate(Turn.LEFT);
				bot.move(1, false);
				continue;
			}
			// if there is no front wall, step forward
			if (bot.distanceToObstacle(Direction.FORWARD) != 0) {
				bot.move(1, false);
				continue;
			}
			// limitation: terminate the program in case of
			// infinite loop
			if (dbg > 10000000) {
				System.out.println("debugging " + dbg + ": infinite loop");
				throw new Exception();
			}
			// otherwise, turn right to look for other path
			System.out.println("Wallfollower: l & f blocked, turn right");
			bot.rotate(Turn.RIGHT);
			
		}
		
		//System.out.println("Wallfollower: exception beyond win/lose");
		//return false;
	}

	// initial energy level is 2000
	@Override
	public float getEnergyConsumption() {
		// TODO Auto-generated method stub
		return 2000f - this.bot.getBatteryLevel();
	}

	@Override
	public int getPathLength() {
		// TODO Auto-generated method stub
		//System.out.println("getPathLength(): " + this.bot.getOdometerReading());
		return this.bot.getOdometerReading();
	}
	
	///////////////////private methods for testing////////////
	protected int getWidth() {
		return this.width;
	}
	
	protected int getHeight() {
		return this.height;
	}
	
	protected Distance getDist() {
		return this.dist;
	}
	
//	protected void debug() {
//		if (this.bot == null) {
//			System.out.println("robot == null");
//		}
//		if (this.bot != null) {
//			System.out.println("robot != null");
//		}
//	}

}
