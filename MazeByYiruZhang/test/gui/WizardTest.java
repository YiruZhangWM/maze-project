package gui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import generation.Cells;
import generation.Distance;
import generation.MazeConfiguration;
import gui.Robot.Turn;

/**
 * Mostly revised from WallFollowerTest, because both classes
 * are RobotDriver. In principle, these two classes can be 
 * used interchangeably to represent different algorithms 
 * to one solution.
 * @author Yiru Zhang
 *
 */
public class WizardTest extends Wizard {
	// private variables
	Robot bot;
	Wizard driver;
	Cells cellTest;
	Controller control;
	MazeConfiguration config;
	
	@Before
	public void setUp() {			
		bot = new BasicRobot();
		driver = new Wizard();	//set driver to Wizard
		control = new Controller();
		driver.setRobot(bot);
		
		control.setFileName("test/data/input.xml");
		control.setRobotAndDriver(bot, driver);
		control.setTesting(true);
		bot.setMaze(control);		
		control.start();
		((BasicRobot) bot).init();
		config = control.getMazeConfiguration();
	}
	
	/**
	 * Test case: correctness of the get/set distance method
	 * <p>
	 * Method under test: setDistance()
	 * <p>
	 * Correct behavior: should return null before distance is 
	 * assigned to the driver; should not return null after a distance
	 * object is assigned to the driver
	 */
	@Test
	public final void testSetDist() {
		assertNull(driver.getDist());
		driver.setDistance(new Distance(1, 1));
		assertNotNull(driver.getDist());
	}
	
	/**
	 * Test case: correctness of the energy consumption level
	 * <p>
	 * Method under test: self setUp(), float getEnergyConsumption()
	 * <p>
	 * Correct behavior: initial energyConsumed should be 0; other
	 * values of energyConsumed should depend on the specific
	 * operations performed on the robot.
	 */
	@Test
	public final void testEnengyConsumptionSimple() {
		// initial condition: energyConsumed == 0
		assertTrue(driver.getEnergyConsumption() == 0f);
		
		// make a move, energyConsumed should change
		bot.move(1, true);
		// System.out.println("driver.getEnergyConsumption(): " + driver.getEnergyConsumption());
		assertTrue(driver.getEnergyConsumption() == 4f);
		
		// make several more move and rotate movements
		// new energy consumed: (3+1)*move + rotate = 22
		bot.move(3, true);
		bot.rotate(Turn.AROUND);
		bot.move(1, true);
		assertTrue(driver.getEnergyConsumption() == 26f);
	}
	
	/**
	 * Test case: correctness of the path length
	 * <p>
	 * Method under test: self setUp(), int getPathLength()
	 * <p>
	 * Correct behavior: initial pathLength should be 0; other
	 * values of pathLength should depend on the specific
	 * operations performed on the robot.
	 */
	@Test
	public final void testPathLengthSimple() {
		assertEquals(driver.getPathLength(), 0);
		
		// make a rotate, pathLen should not change
		bot.rotate(Turn.LEFT);
		assertEquals(driver.getPathLength(), 0);
		bot.rotate(Turn.RIGHT);
		assertEquals(driver.getPathLength(), 0);
		bot.rotate(Turn.AROUND);
		assertEquals(driver.getPathLength(), 0);
		bot.rotate(Turn.AROUND);
		
		// make movements, pathLen should change
		bot.move(1, true);
		assertNotEquals(driver.getPathLength(), 0);
		assertEquals(driver.getPathLength(), 1);
		bot.move(6, true);
		assertEquals(driver.getPathLength(), 7);
		bot.rotate(Turn.LEFT);
		bot.move(1, true);
		assertEquals(driver.getPathLength(), 7);
		bot.rotate(Turn.LEFT);
		bot.move(3, true);
		assertEquals(driver.getPathLength(), 10);
		
		// reset odometer
		bot.resetOdometer();
		assertEquals(driver.getPathLength(), 0);
	}
	
	/**
	 * Test case: correctness of the get/set of dimension
	 * <p>
	 * Method under test: setDimensions(int width, int height)
	 * <p>
	 * Correct behavior: set the correct width and height as required
	 */
	@Test
	public final void testSetDimensions() {
		assertEquals(driver.getWidth(), 0);
		assertEquals(driver.getHeight(), 0);
		driver.setDimensions(3, 5);
		assertEquals(driver.getWidth(), 3);
		assertEquals(driver.getHeight(), 5);
	}
	
	/**
	 * Test case: correctness of the battery handling in the 
	 * drive2Exit() method
	 * <p>
	 * Method under test: boolean drive2Exit()
	 * <p>
	 * Correct behavior: artificially leak the battery to have
	 * impossible battery to find a way out. See if the method being
	 * tested can successfully throw an exception.
	 * @throws Exception 
	 */
	@Test
	public final void testD2EInsufficientBattery() throws Exception {
		boolean testHelper = false;
		// manually leaks the battery
		bot.setBatteryLevel(10f);
		try {
			driver.drive2Exit();
		}
		catch (Exception e) {
			testHelper = true;
		}
		finally {
			assertTrue(testHelper);
		}
		
	}
	
	/**
	 * Test case: besides the correctness of the drive2Exit()
	 * method, more brunch of the private turnTo() method 
	 * can be tested by providing more batteries to see if each
	 * conditional works as intended.
	 * <p>
	 * Method under test: boolean drive2Exit()
	 * <p>
	 * Correct behavior: artificially leak the battery to have
	 * more but still insufficient battery to find a way out. 
	 * See if the method being
	 * tested can successfully throw an exception.
	 * @throws Exception 
	 */
	@Test
	public final void testD2EMoreBattery() throws Exception {
		boolean testHelper = false;
		// manually leaks the battery, but to a greater level
		bot.setBatteryLevel(500f);
		try {
			driver.drive2Exit();
		}
		catch (Exception e) {
			testHelper = true;
		}
		finally {
			assertTrue(testHelper);
		}
		
	}
	
	/**
	 * Test case: 
	 * <p>
	 * Method under test: 
	 * <p>
	 * Correct behavior: 
	 * @throws Exception 
	 */
//	@Test
//	public final void testMoban() throws Exception {
//		driver.drive2Exit();
//	}
}
