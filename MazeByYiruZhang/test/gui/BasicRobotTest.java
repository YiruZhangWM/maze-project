package gui;

import gui.Controller;
import gui.Robot.Direction;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import generation.CardinalDirection;
import generation.Cells;
import generation.MazeConfiguration;

/**
 * Tests BasicRobot class using a fixed maze (input.xml)
 * starting position is always (4, 0), 
 * starting CardinalDirection is always East.
 * @author Yiru Zhang
 *
 */
public class BasicRobotTest extends BasicRobot {
	
	// private variables
//	BasicRobot bot;
	Robot bot;
	RobotDriver driver;
	Cells cellTest;
	Controller control;
	MazeConfiguration config;
	
	/**
	 * Set up a controller and use the controller to set up
	 * a BasicRobot.
	 * @throws Exception  
	 */
	@Before
	public void setUp() {
		bot = new BasicRobot();	
		control = new Controller();
		driver = null;	//manual mode
		
		control.setFileName("test/data/input.xml");
		control.setRobotAndDriver(bot, driver);
		
		bot.setMaze(control);		
		control.start();
		((BasicRobot) bot).init();
		
		config = control.getMazeConfiguration();
	}
	
	/**
	 * Test case: test if the starting stats are correct. Since
	 * an input maze is read, the starting position is fixed and
	 * should be the consistent with the fetched value.
	 * <p>
	 * Method under test: getCurrentPosition(), 
	 * getCurrentDirection(), getBatteryLevel()
	 * <p>
	 * Correct behavior: starting position should be (4, 0),
	 * starting Cardinal direction should be East,
	 * starting batteryLevel should be 2000f
	 * @throws Exception 
	 */
	@Test
	public final void testStartValue() throws Exception {
		int x, y;
		x = bot.getCurrentPosition()[0];
		y = bot.getCurrentPosition()[1];
		assertEquals(x, 4);
		assertEquals(y, 0);
		assertEquals(bot.getCurrentDirection(),
					 CardinalDirection.East);
		assertTrue(bot.getBatteryLevel() == 2000f);
	}
	
	/**
	 * Test case: correctness of rotate and its internal management
	 * of currentPosition, currentDirection, and batteryLevel.
	 * <p>
	 * Method under test: rotate(), getCurrentPosition(), 
	 * getCurrentDirection(), getBatteryLevel()
	 * <p>
	 * Correct behavior: rotate for the correct direction, 
	 * currentPosition should not change, batteryLevel should 
	 * record the intended value.
	 * Starting position is fixed to (4, 0) (determined by the 
	 * input.xml file)
	 * @throws Exception 
	 */
	@Test
	public final void testRotation() throws Exception {
		// initial conditions
		assertEquals(bot.getCurrentPosition()[0], 4);  //x coord
		assertEquals(bot.getCurrentPosition()[1], 0);  //y coord
		assertEquals(bot.getCurrentDirection(),
					 CardinalDirection.East);
		assertTrue(bot.getBatteryLevel() == 2000f);
		
		// First, rotate left
		bot.rotate(Turn.LEFT);
		System.out.println("curDir is: " + bot.getCurrentDirection());
		// coordinate should not change for a rotate() operation
		assertEquals(bot.getCurrentPosition()[0], 4);  //x
		assertEquals(bot.getCurrentPosition()[1], 0);  //y
		assertTrue(bot.getBatteryLevel() == 1997f);
		assertEquals(bot.getCurrentDirection(),
					 CardinalDirection.South);
		
		// Second, rotate right (to the original position)
		bot.rotate(Turn.RIGHT);
		assertEquals(bot.getCurrentPosition()[0], 4);  //x
		assertEquals(bot.getCurrentPosition()[1], 0);  //y
		assertEquals(bot.getCurrentDirection(),
					 CardinalDirection.East);
		assertTrue(bot.getBatteryLevel() == 1994f);
		
		// Then, rotate around
		bot.rotate(Turn.AROUND);
		assertEquals(bot.getCurrentPosition()[0], 4);  //x
		assertEquals(bot.getCurrentPosition()[1], 0);  //y
		assertEquals(bot.getCurrentDirection(),
					 CardinalDirection.West);
		assertTrue(bot.getBatteryLevel() == 1988f);
	}
	
	/**
	 * Test case: correctness of the battery handling of the 
	 * rotate() method
	 * <p>
	 * Method under test: rotate()
	 * <p>
	 * Correct behavior: hasStopped() should return true, 
	 * batteryLevel should be set to 0.
	 * @throws Exception 
	 */
	@Test
	public final void testRotationBatteryLoss() {
		// test initial conditions first
		assertFalse(bot.hasStopped());
		assertNotEquals(bot.getBatteryLevel(), 0f);
		
		// set the battery to insufficient battery = 1.0
		bot.setBatteryLevel(1f);
		// these conditions should not change
		assertFalse(bot.hasStopped());
		assertTrue(bot.getBatteryLevel() == 1f);
		
		// perform a rotation
		bot.rotate(Turn.LEFT);
		assertTrue(bot.hasStopped());
		assertTrue(bot.getBatteryLevel() == 0f);
	}
	
	/**
	 * Test case: correctness of the move operation including
	 * correct currentPosition, correct currentDirection, 
	 * correct control of the battery level
	 * <p>
	 * Method under test: move(int distance, false)
	 * <p>
	 * Correct behavior: reflect the correct values for 
	 * currentPosition, currentDirection, batteryLevel
	 * @throws Exception 
	 */
	@Test
	public final void testSimpleMoveManually() throws Exception {
		int x, y;
		
		// initial conditions
		assertEquals(bot.getOdometerReading(), 0);	//initial odometer
		assertEquals(bot.getCurrentPosition()[0], 4);  //x coord
		assertEquals(bot.getCurrentPosition()[1], 0);  //y coord
		assertEquals(bot.getCurrentDirection(),
					 CardinalDirection.East);
		assertTrue(bot.getBatteryLevel() == 2000f);
		
		// move 1 step forward
		bot.move(1, false);
//		x = bot.getCurrentPosition()[0];
//		y = bot.getCurrentPosition()[1];
//		System.out.println("im here (x, y) is: (" + x + ", " + y + ")");

		assertEquals(bot.getCurrentPosition()[0], 5);  //x
		assertEquals(bot.getCurrentPosition()[1], 0);  //y
		assertEquals(bot.getCurrentDirection(),
					 CardinalDirection.East);
		assertTrue(bot.getBatteryLevel() == 1996f);
		
		// turn around and walk up to the corner
		bot.rotate(Turn.AROUND);
		assertEquals(bot.getCurrentDirection(),
				 CardinalDirection.West);
		bot.move(1, false);
		assertEquals(bot.getCurrentPosition()[0], 4);  //x
		assertEquals(bot.getCurrentPosition()[1], 0);  //y
		assertEquals(bot.getCurrentDirection(),
					 CardinalDirection.West);
		
		// keep moving 1 step forward. Since a wall is in front,
		// nothing should change
		bot.move(1, false);
		assertEquals(bot.getCurrentPosition()[0], 4);  //x
		assertEquals(bot.getCurrentPosition()[1], 0);  //y
		assertEquals(bot.getCurrentDirection(),
					 CardinalDirection.West);
		
		// if a driver is used, hasStop needs to change
		bot.move(1, true);
		assertEquals(bot.getCurrentPosition()[0], 4);  //x
		assertEquals(bot.getCurrentPosition()[1], 0);  //y
		assertEquals(bot.getCurrentDirection(),
					 CardinalDirection.West);
		assertTrue(bot.hasStopped());
	}
	
	/**
	 * Test case: correctness of the resetOdometer operation given that
	 * a move operation is valid
	 * <p>
	 * Method under test: move(int distance, false), resetOdometer()
	 * <p>
	 * Correct behavior: for a given nonzero odometer reading,
	 * the reset method should reset the odometer to 0.
	 */
	@Test
	public final void testSetResetOdometerForMoveOp() {
		// initial conditions
		assertFalse(bot.hasStopped());
		assertEquals(bot.getOdometerReading(), 0);
		assertTrue(bot.getBatteryLevel() == 2000f);
		
		// manually leak the battery to 4
		bot.setBatteryLevel(4f);
		bot.move(1, false);
		assertFalse(bot.hasStopped());
		assertNotEquals(bot.getOdometerReading(), 0);
		assertTrue(bot.getBatteryLevel() == 0f);
		
		// reset the odometer and checks for performance
		bot.resetOdometer();
		assertEquals(bot.getOdometerReading(), 0);
	}
	
	/**
	 * Test case: correctness of the distanceToObstacle() method
	 * <p>
	 * Method under test: int distanceToObstacle(Direction direction)
	 * <p>
	 * Correct behavior: give the correct value
	 */
	@Test
	public final void testDist2Obstacle() {
		int d0, d;
		d0 = bot.distanceToObstacle(Direction.BACKWARD);
		System.out.println("initial dist2obstacle is: " + d0);
		assertEquals(d0, 0);
		
		bot.move(1, false);
		d = bot.distanceToObstacle(Direction.BACKWARD);
		System.out.println("dist2obstacle is: " + d);
		assertEquals(d, 1);
		assertEquals(bot.distanceToObstacle(Direction.FORWARD), 10);
		
		bot.rotate(Turn.LEFT);
		assertEquals(bot.distanceToObstacle(Direction.RIGHT), 10);
		assertEquals(bot.distanceToObstacle(Direction.LEFT), 1);
		bot.rotate(Turn.RIGHT);
		
		bot.move(10, false);
		assertEquals(bot.distanceToObstacle(Direction.FORWARD), 0);
		assertEquals(bot.distanceToObstacle(Direction.BACKWARD), 11);
		d = bot.distanceToObstacle(Direction.LEFT);
		System.out.println("left: dist2obstacle is: " + d);
		assertEquals(bot.distanceToObstacle(Direction.LEFT), 2);
		d = bot.distanceToObstacle(Direction.RIGHT);
		System.out.println("right: dist2obstacle is: " + d);
		assertEquals(bot.distanceToObstacle(Direction.RIGHT), 0);
		
	}
	
	/**
	 * Test case: test if getEnergyForFullRotation() returns the correct
	 * value
	 * <p>
	 * Method under test: self setUp(), getEnergyForFullRotation()
	 * <p>
	 * Correct behavior: the method should return 12.0f
	 */
	@Test
	public final void testGetEnergyForFullRotation() {
		boolean ok = getEnergyForFullRotation() == 12.0f;
		assertTrue(ok);
	}
	
	/**
	 * Test case: test if getEnergyForStepForward() returns the correct
	 * value
	 * <p>
	 * Method under test: self setUp(), getEnergyForStepForward()
	 * <p>
	 * Correct behavior: the method should return 4.0f
	 */
	@Test
	public final void testGetEnergyForStepForward() {
		boolean ok = getEnergyForStepForward() == 4.0f;
		assertTrue(ok);
	}
	
	/**
	 * Test case: test if the bot has a room sensor
	 * <p>
	 * Method under test: boolean hasRoomSensor()
	 * <p>
	 * Correct behavior: should always return true
	 * @throws Exception 
	 */
	@Test
	public final void testRoomSensorExist() {
		assertTrue(bot.hasRoomSensor());
	}
	/**
	 * Test case: test if the bot has distance sensors for all 
	 * four directions
	 * <p>
	 * Method under test: boolean hasDistanceSensor(Direction direction)
	 * <p>
	 * Correct behavior: should be true for all four because
	 * the default setting is true
	 * @throws Exception 
	 */
	@Test
	public final void testDistSensorExist() {
		assertTrue(bot.hasDistanceSensor(Direction.LEFT));
		assertTrue(bot.hasDistanceSensor(Direction.FORWARD));
		assertTrue(bot.hasDistanceSensor(Direction.RIGHT));
		assertTrue(bot.hasDistanceSensor(Direction.BACKWARD));
	}
	
	/**
	 * Test case: test if the room indicator works as expected
	 * <p>
	 * Method under test: boolean isInsideRoom()
	 * <p>
	 * Correct behavior: return false for the initial position
	 * is not inside a room
	 * @throws Exception 
	 */
	@Test
	public final void testIsInsideRoom() throws Exception {
		assertFalse(bot.isInsideRoom());
		
		// move into a room
		bot.move(11, false);
		bot.rotate(Turn.LEFT);
		bot.move(2, false);
		bot.rotate(Turn.RIGHT);
		assertFalse(bot.isInsideRoom());
		bot.move(1, false);
		assertTrue(bot.isInsideRoom());
		bot.move(1, false);
		assertTrue(bot.isInsideRoom());
	}
	
	/**
	 * Test case: test if the get/set battery level works correctly
	 * <p>
	 * Method under test: setBatteryLevel(float level), 
	 * int getOdometerReading()
	 * <p>
	 * Correct behavior: get/set works as intended
	 */
	@Test
	public final void testGetSetBatteryLevel() {
		assertFalse(bot.getBatteryLevel() == 0);
		bot.setBatteryLevel(0f);
		assertTrue(bot.getBatteryLevel() == 0);
	}
	
	/**
	 * Test case: correctness of the get exit method
	 * <p>
	 * Method under test: boolean isAtExit()
	 * <p>
	 * Correct behavior: true if at exit; false otherwise
	 * @throws Exception 
	 */
	@Test
	public final void testIsAtExit() throws Exception {
		assertFalse(bot.isAtExit());
//		int x, y;
//		MazeConfiguration config1;
//		BasicRobot bot1 = new BasicRobot();	
//		Controller control1 = new Controller();
//		WallFollower driver1 = null;	//manual mode
//		control1.setFileName("test/data/input.xml");
//		control1.setRobotAndDriver(bot1, driver1);
//		config1 = control.getMazeConfiguration();
//		System.out.println("(before) config1 == null is: ");
//		System.out.println(config1 == null);
//		bot1.setMaze(control1);		
//		control1.start();
//		config1 = control.getMazeConfiguration();
//		System.out.println("(after start) config1 == null is: ");
//		System.out.println(config1 == null);
//		config1.setStartingPosition(0, 3);
//		((BasicRobot) bot1).init();
//		System.out.println("START TESTING");		
//		x = bot1.getCurrentPosition()[0];
//		y = bot1.getCurrentPosition()[1];
//		System.out.println("(x, y) is: (" + x + ", " + y + ")");
//		assertTrue(bot.isAtExit());
	}
	
	/**
	 * Test case: correctness of the can-see-exit method
	 * <p>
	 * Method under test: boolean canSeeExit(Direction direction)
	 * <p>
	 * Correct behavior: true if exit is visible; false otherwise
	 * @throws Exception 
	 */
	@Test
	public final void testCanSeeExit() throws Exception {
		assertFalse(bot.canSeeExit(Direction.BACKWARD));
		assertFalse(bot.canSeeExit(Direction.FORWARD));
		assertFalse(bot.canSeeExit(Direction.LEFT));
		assertFalse(bot.canSeeExit(Direction.RIGHT));
	}
	
	
	/**
	 * Test case: 
	 * <p>
	 * Method under test: 
	 * <p>
	 * Correct behavior: 
	 */
//	@Test
//	public final void testMoban() {
//	
//	}
}
