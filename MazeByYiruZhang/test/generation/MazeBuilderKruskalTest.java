package generation;

import static org.junit.Assert.*;
import generation.Order.Builder;
import generation.MazeBuilderKruskal;
import generation.CellsCoord;
import org.junit.Before;
import org.junit.Test;

/**
 * Test file for MazeBuilderKruskal.
 * Mainly test if the class and its methods really works as intended.
 * @author Yiru Zhang
 *
 */
public class MazeBuilderKruskalTest extends MazeBuilderKruskal{
	
	//private variables
	private int x1, y1, x2, y2;
	private CellsCoord cc1, cc2;
	private MazeBuilderKruskal mk;
	
	//these variables are the same with MazeFactoryTests.java
	private OrderObject mockOrder, mockOrder2;
	private MazeFactory mazeFactDtm, mazeFactDtm2;
	private int skillLevel = 0;
	private Builder builder;
	
	/**
	 * 
	 */
	@Before
	public void setUp() {
		mk = new MazeBuilderKruskal();
		
		// from MazeFactoryTests.java
		mockOrder = new OrderObject(skillLevel);
		mockOrder2 = new OrderObject(skillLevel);
		
		mockOrder.setBuilder(Builder.Kruskal);
		mockOrder2.setBuilder(Builder.Kruskal);
//		mockOrder.setBuilder(Builder.Prim);
//		mockOrder2.setBuilder(Builder.Prim);
		
		mazeFactDtm = new MazeFactory(true);
		mazeFactDtm2 = new MazeFactory(true);
	}
	
	/**
	 * Test case: CellsCoord class constructor test and get 
	 * method test
	 * <p>
	 * Methods under test: self setUp(), CellsCoord.getX(), 
	 * CellsCoord.getY()
	 * <p>
	 * Correct behavior: get the correct value of x and y
	 */
	@Test
	public final void testCellsCoordClassGetMethod() {
		x1 = 1; y1 = 2;
		cc1 = new CellsCoord(x1, y1);
		assertEquals(cc1.getX(), 1);
		assertEquals(cc1.getY(), 2);
	}
	
	/**
	 * Test case: CellsCoord class constructor test and set 
	 * method test
	 * <p>
	 * Methods under test: self setUp(), CellsCoord.getX(), 
	 * CellsCoord.getY(), CellsCoord.setX(), CellsCoord.setY()
	 * <p>
	 * Correct behavior: be able to set and get the correct value of 
	 * x and y
	 */
	@Test
	public final void testCellsCoordClassSetMethod() {
		x1 = 1; y1 = 2;
		cc1 = new CellsCoord(x1, y1);
		assertEquals(cc1.getX(), 1);
		assertEquals(cc1.getY(), 2);
		
		cc1.setX(3); cc1.setY(4);
		assertEquals(cc1.getX(), 3);
		assertEquals(cc1.getY(), 4);
	}
	
	/**
	 * Test case: test if the equals() method in CellsCoord class 
	 * is able to run correctly
	 * <p>
	 * Methods under test: self setUp(), CellsCoord constructor,
	 * CellsCoord.equals(CellsCoord cc1, CellsCoord cc2)
	 * <p>
	 * Correct behavior: be able to tell if two CellsCoord are equal
	 */
	@Test
	public final void testCellsCoordEquals() {
		x1 = 1; y1 = 2;
		x2 = 1; y2 = 2;
		cc1 = new CellsCoord(x1, y1);
		assertFalse(cc1.equals(cc2));
		cc2 = new CellsCoord(x2, y2);
		assertTrue(cc1.equals(cc2));
		
		//now make some changes
		cc1.setX(3);
		assertFalse(cc1.equals(cc2));
		cc2.setX(3);
		assertTrue(cc1.equals(cc2));
		cc1.setY(4);
		assertFalse(cc1.equals(cc2));
		cc2.setY(4);
		assertTrue(cc1.equals(cc2));
	}
	
	/**
	 *  Test case: see if the constructor used in setUp delivers anything
	 *  <p>
	 *  Method under test: self setUp(), MazeBuilderKruskal constructor
	 *  <p>
	 *  Correct behavior: It is correct if the maze factory field 
	 *  is not null
	 */
	@Test
	public final void testKruskalMazeFactNullity() {
		assertNotNull(mazeFactDtm);
		assertNotNull(mazeFactDtm2);
	}
	
//	public final void testXiaTest() {
//		mk.generate();
//	}
	
	/**
	 * Test case: see if the deterministic characteristic is present 
	 * in the Kruskal setup
	 * <p>
	 * Method under test: self setUp(), MazeBuilderKruskal constructor
	 * <p>
	 * Correct behavior: correct if the two maze factory are equal
	 */
	@Test
	public final void testKruskalDeterministicSetup() {
		assertNotEquals(mazeFactDtm, mazeFactDtm2);
		mazeFactDtm.order(mockOrder);
		mazeFactDtm.waitTillDelivered();
		mazeFactDtm2.order(mockOrder);
		mazeFactDtm2.waitTillDelivered();
		MazeConfiguration mazeConfig = mockOrder.getMazeConfig();
		MazeConfiguration mazeConfig2 = mockOrder.getMazeConfig();
		assertNotNull(mazeConfig);
		assertEquals(mazeConfig.getWidth(), mazeConfig2.getWidth());
		assertEquals(mazeConfig.getHeight(), mazeConfig2.getHeight());
		
		for (int x = 0; x < mazeConfig.getWidth(); ++x) {
			for (int y = 0; y < mazeConfig.getHeight(); ++y) {
				assertEquals(mazeConfig.getDistanceToExit(x, y),
							 mazeConfig2.getDistanceToExit(x, y));
			}
		}
	}
	
	/**
	 * Test case: see if MazeBuilderKruskal works by applying the 
	 * algorithm to the 4x4 maze (skill level = 0)
	 * <p>
	 * Method under test: MazeBuilderKruskal constructor, 
	 * MazeBuilderKruskal generatePathways()
	 * <p>
	 * Correct behavior: correct if and only if exactly one cell have 
	 * its distance to exit to be 1.
	 */
	@Test
	public final void testIfExitExistsSkill0() {
		skillLevel = 0;
		assertTrue(mazeFactDtm.order(mockOrder));
		mazeFactDtm.waitTillDelivered();

		MazeConfiguration mazeConfig = mockOrder.getMazeConfig();
		//Distance distOfMock = mazeConfig.getMazedists();
		int exitCount = 0;
		
		for (int x = 0; x < mazeConfig.getWidth(); ++x) {
			for (int y = 0; y < mazeConfig.getHeight(); ++y) {
				//if (distOfMock.getDistanceValue(x, y) == 1) {
				if (mazeConfig.getDistanceToExit(x, y) == 1) {
					exitCount++;
				}
			}
		}
		assertEquals(1, exitCount);
	}
	
	
//	/**
//	 * Test case: 
//	 * <p>
//	 * Methods under test: 
//	 * <p>
//	 * Correct behavior: 
//	 */
//	@Test
//	public final void testMoban() {
//		
//	}

}
