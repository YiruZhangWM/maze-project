package generation;

import generation.Order;
import gui.Constants;

/**
 * OrderObject provides a class that is used for instantiating a Order
 * object for test in MazeFactoryTest 
 */
public class OrderObject implements Order{
	public MazeConfiguration mazeConfig;
	public int perc;
	public int skill;
	public Builder builder;
	public boolean isPerfect;
	
	
	public OrderObject(int skill) {
		//mazeConfig = new MazeContainer();
		perc = 0;
		this.skill = skill;
		isPerfect = false;
		builder = Order.Builder.DFS;
		
		//mazeConfig.setWidth(Constants.SKILL_X[skill]);
		//mazeConfig.setHeight(Constants.SKILL_Y[skill]);
	}

	@Override
	public int getSkillLevel() {
		// TODO Auto-generated method stub
		return this.skill;
	}
	
	public void setSkillLevel(int skill) {
		this.skill = skill;
	}

	@Override
	public Builder getBuilder() {
		// TODO Auto-generated method stub
		return this.builder;
	}
	
	public void setBuilder(Builder b) {
		this.builder = b;
	}

	@Override
	public boolean isPerfect() {
		// TODO Auto-generated method stub
		return this.isPerfect;
	}
	
	public void setIsPerfect(boolean ip) {
		this.isPerfect = ip;
	}
	
	public MazeConfiguration getMazeConfig() {
		return this.mazeConfig;
	}

	@Override
	public void deliver(MazeConfiguration mazeConfig) {
		// TODO Auto-generated method stub
		//System.out.println("OrderObject.deliver(): nothing happens");
		this.mazeConfig = mazeConfig;
	}

	@Override
	public void updateProgress(int percentage) {
		// TODO Auto-generated method stub
		//System.out.println("OrderObject.updateProgress(): nothing happens");
		return;
	}
}
