package generation;

import static org.junit.Assert.*;

import generation.MazeBuilder;
import generation.MazeBuilderPrim;
//import generation.MazeBuilderKruskal;
import generation.Order;
import generation.Order.Builder;

//import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test the MazeBuilder class which one can then easily reuse 
 * and adjust for the new algorithm with the help of inheritance.
 * <p>
 * The current setting in this test file tests for Kruskal's
 * algorithm's implementation. 
 * <p>
 * To see if the test case works for a different algorithm, change
 * the comment on line 47 to 50 to modify algorithms.
 * 1. DFS: comment all four lines;
 * 2. Prim: uncomment line 49 and line 50;
 * 3. Kruskal (default setting): uncomment line 47 and line 48;
 * @author Yiru Zhang
 *
 */
public class MazeFactoryTest {
	
	//private variables
	private OrderObject mockOrder, mockOrder2;
	private MazeFactory mazeFactDtm, mazeFactDtm2;
	private int skillLevel = 3;
	private Builder builder;
	
	/**
	 * Create a mock Order object for testing;
	 * create a deterministic maze using the MazeFactory.
	 */
	@Before
	public void setUp() {
		mockOrder = new OrderObject(skillLevel);
		mockOrder2 = new OrderObject(skillLevel);
		
//		mockOrder.setBuilder(Builder.Prim);
//		mockOrder2.setBuilder(Builder.Prim);
		mockOrder.setBuilder(Builder.Kruskal);
		mockOrder2.setBuilder(Builder.Kruskal);
		
		mazeFactDtm = new MazeFactory(true);
		mazeFactDtm2 = new MazeFactory(true);

	}
	

	/**
	 *  Test case: see if the constructor used in setUp delivers anything
	 *  <p>
	 *  Method under test: self setUp();
	 *  <p>
	 *  Correct behavior: It is correct if the maze factory field is not null
	 */
	@Test
	public final void testMazeFactNullity() {
		assertNotNull(mazeFactDtm);
		assertNotNull(mazeFactDtm2);
	}
	
	/**
	 * Test case: see if the deterministic characteristic is there
	 * <p>
	 * Method under test: self setUp()
	 * <p>
	 * Correct behavior: correct if the two maze factory are equal
	 */
	@Test
	public final void testOfDeterministicSetup() {
		assertNotEquals(mazeFactDtm, mazeFactDtm2);
		mazeFactDtm.order(mockOrder);
		mazeFactDtm.waitTillDelivered();
		mazeFactDtm2.order(mockOrder);
		mazeFactDtm2.waitTillDelivered();
		MazeConfiguration mazeConfig = mockOrder.getMazeConfig();
		MazeConfiguration mazeConfig2 = mockOrder.getMazeConfig();
		
		assertEquals(mazeConfig.getWidth(), mazeConfig2.getWidth());
		assertEquals(mazeConfig.getHeight(), mazeConfig2.getHeight());
		
		for (int x = 0; x < mazeConfig.getWidth(); ++x) {
			for (int y = 0; y < mazeConfig.getHeight(); ++y) {
				assertEquals(mazeConfig.getDistanceToExit(x, y),
							 mazeConfig2.getDistanceToExit(x, y));
			}
		}
	}
	
	
	/**
	 * Test case: since testing all skill levels takes quite significant
	 * amount of time. Only skill levels of 0, 5, 10, and 15 are selected
	 * to represent the results.
	 * <p>
	 * See if the maze has an correct exit by checking if 
	 * mazeFactDtm.dists has the correct number of elements equal to 1 
	 * (skill level = 0)
	 * <p>
	 * Method under test: MazeFactory(true), MazeFactory.order(Order order), 
	 * MazeConfiguration.getWidth(), MazeConfiguration.getHeight(), 
	 * MazeConfiguration.getDistanceToExit(int x, int y).
	 * <p>
	 * Correct behavior: correct if and only if exactly one cell have 
	 * its distance to exit to be 1.
	 */
	@Test
	public final void testIfExitExistsSkill0() {
		skillLevel = 0;
		assertTrue(mazeFactDtm.order(mockOrder));
		mazeFactDtm.waitTillDelivered();

		MazeConfiguration mazeConfig = mockOrder.getMazeConfig();
		//Distance distOfMock = mazeConfig.getMazedists();
		int exitCount = 0;
		
		for (int x = 0; x < mazeConfig.getWidth(); ++x) {
			for (int y = 0; y < mazeConfig.getHeight(); ++y) {
				//if (distOfMock.getDistanceValue(x, y) == 1) {
				if (mazeConfig.getDistanceToExit(x, y) == 1) {
					exitCount++;
				}
			}
		}
		assertEquals(1, exitCount);
		
	}
	
	/**
	 * Test case: see if the maze has an correct exit by checking if 
	 * mazeFactDtm.dists has the correct number of elements equal to 1 
	 * (skill level = 5)
	 * <p>
	 * Method under test: MazeFactory(true), MazeFactory.order(Order order), 
	 * MazeConfiguration.getWidth(), MazeConfiguration.getHeight(), 
	 * MazeConfiguration.getDistanceToExit(int x, int y).
	 * <p>
	 * Correct behavior: correct if and only if exactly one cell have 
	 * its distance to exit to be 1.
	 */
	@Test
	public final void testIfExitExistsSkill5() {
		skillLevel = 5;
		assertTrue(mazeFactDtm.order(mockOrder));
		mazeFactDtm.waitTillDelivered();

		MazeConfiguration mazeConfig = mockOrder.getMazeConfig();
		//Distance distOfMock = mazeConfig.getMazedists();
		int exitCount = 0;
		
		for (int x = 0; x < mazeConfig.getWidth(); ++x) {
			for (int y = 0; y < mazeConfig.getHeight(); ++y) {
				//if (distOfMock.getDistanceValue(x, y) == 1) {
				if (mazeConfig.getDistanceToExit(x, y) == 1) {
					exitCount++;
				}
			}
		}
		assertEquals(1, exitCount);
		
	}
	
	/**
	 * Test case: see if the maze has an correct exit by checking if 
	 * mazeFactDtm.dists has the correct number of elements equal to 1 
	 * (skill level = 10)
	 * <p>
	 * Method under test: MazeFactory(true), MazeFactory.order(Order order), 
	 * MazeConfiguration.getWidth(), MazeConfiguration.getHeight(), 
	 * MazeConfiguration.getDistanceToExit(int x, int y).
	 * <p>
	 * Correct behavior: correct if and only if exactly one cell have 
	 * its distance to exit to be 1.
	 */
	@Test
	public final void testIfExitExistsSkill10() {
		skillLevel = 10;
		assertTrue(mazeFactDtm.order(mockOrder));
		mazeFactDtm.waitTillDelivered();

		MazeConfiguration mazeConfig = mockOrder.getMazeConfig();
		//Distance distOfMock = mazeConfig.getMazedists();
		int exitCount = 0;
		
		for (int x = 0; x < mazeConfig.getWidth(); ++x) {
			for (int y = 0; y < mazeConfig.getHeight(); ++y) {
				//if (distOfMock.getDistanceValue(x, y) == 1) {
				if (mazeConfig.getDistanceToExit(x, y) == 1) {
					exitCount++;
				}
			}
		}
		assertEquals(1, exitCount);
		
	}
	
	/**
	 * Test case: see if the maze has an correct exit by checking if 
	 * mazeFactDtm.dists has the correct number of elements equal to 1 
	 * (skill level = 15)
	 * <p>
	 * Method under test: MazeFactory(true), MazeFactory.order(Order order), 
	 * MazeConfiguration.getWidth(), MazeConfiguration.getHeight(), 
	 * MazeConfiguration.getDistanceToExit(int x, int y).
	 * <p>
	 * Correct behavior: correct if and only if exactly one cell have 
	 * its distance to exit to be 1.
	 */
	@Test
	public final void testIfExitExistsSkill15() {
		skillLevel = 15;
		assertTrue(mazeFactDtm.order(mockOrder));
		mazeFactDtm.waitTillDelivered();

		MazeConfiguration mazeConfig = mockOrder.getMazeConfig();
		//Distance distOfMock = mazeConfig.getMazedists();
		int exitCount = 0;
		
		for (int x = 0; x < mazeConfig.getWidth(); ++x) {
			for (int y = 0; y < mazeConfig.getHeight(); ++y) {
				//if (distOfMock.getDistanceValue(x, y) == 1) {
				if (mazeConfig.getDistanceToExit(x, y) == 1) {
					exitCount++;
				}
			}
		}
		assertEquals(1, exitCount);
		
	}
	
	/**
	 * Test case: test if the maze has every coordinate to be valid
	 * <p>
	 * Method under test: MazeFactory(true), MazeFactory.order(Order order), 
	 * MazeConfiguration.getDistanceToExit(int x, int y)
	 * <p>
	 * Correct behavior: every single cell should have a distance to exit 
	 * to be strictly positive.
	 */
	@Test
	public final void testIfEveryCellIsValid() {
		assertTrue(mazeFactDtm.order(mockOrder));
		mazeFactDtm.waitTillDelivered();
		MazeConfiguration mazeConfig = mockOrder.getMazeConfig();
		boolean isValid = true;
		
		for (int x = 0; x < mazeConfig.getWidth(); ++x) {
			for (int y = 0; y < mazeConfig.getHeight(); ++y) {
				if (mazeConfig.getDistanceToExit(x, y) < 1) {
					isValid = false;
					x = mazeConfig.getWidth();
					y = mazeConfig.getHeight();
				}
			}
		}
		assertTrue(isValid);
	}
	
	/**
	 * Test case: test if the maze has every coordinate to be valid 
	 * using the isValidPosition(int x, int y) method
	 * <p>
	 * Method under test: MazeFactory(true), MazeFactory.order(Order order), 
	 * MazeConfiguration.getDistanceToExit(int x, int y), 
	 * MazeConfiguration.isValidPosition(int x, int y).
	 * <p>
	 * Correct behavior: every single cell should have a distance to exit 
	 * to be strictly positive.
	 */
	@Test
	public final void testIfEveryCellIsValidWithExistingMethod() {
		assertTrue(mazeFactDtm.order(mockOrder));
		mazeFactDtm.waitTillDelivered();
		MazeConfiguration mazeConfig = mockOrder.getMazeConfig();
		boolean isValid = true;
		
		for (int x = 0; x < mazeConfig.getWidth(); ++x) {
			for (int y = 0; y < mazeConfig.getHeight(); ++y) {
				if (!mazeConfig.isValidPosition(x, y)) {
					isValid = false;
					x = mazeConfig.getWidth();
					y = mazeConfig.getHeight();
				}
			}
		}
		assertTrue(isValid);
	}
	
	/**
	 * Test case: test if the starting position can be reached using 
	 * getStartingPosition() and is not null by default
	 * <p>
	 * Method under test: MazeFactory.order(Order.order), 
	 * MazeConfiguration.getMazeConfig(), mazeConfiguration.getStartingPosition().
	 * <p>
	 * Correct behavior: starting position should not be null
	 */
	@Test
	public final void testStartingPositionExistence() {
		assertTrue(mazeFactDtm.order(mockOrder));
		mazeFactDtm.waitTillDelivered();
		MazeConfiguration mazeConfig = mockOrder.getMazeConfig();
		
		assertNotNull(mazeConfig.getStartingPosition());
	}
	
	/**
	 * Test case: test if the starting position has the greatest distance to 
	 * the exit as specified
	 * <p>
	 * Method under test: MazeFactory.order(Order.order), 
	 * MazeConfiguration.getMazeConfig(), mazeConfiguration.getStartingPosition(), 
	 * MazeConfiguration.getDistanceToExit().
	 * <p>
	 * Correct behavior: starting position should have the greatest distance 
	 * to the exit
	 */
	@Test
	public final void testStartingPositionCorrectness() {
		assertTrue(mazeFactDtm.order(mockOrder));
		mazeFactDtm.waitTillDelivered();
		MazeConfiguration mazeConfig = mockOrder.getMazeConfig();
		
		boolean isTrue = true;
		int x = mazeConfig.getStartingPosition()[0],
			y = mazeConfig.getStartingPosition()[1],
			sDist;
		sDist = mazeConfig.getDistanceToExit(x, y);
		for (int i = 0; i < mazeConfig.getWidth(); ++i) {
			for (int j = 0; j < mazeConfig.getHeight(); ++j) {
				if (mazeConfig.getDistanceToExit(i, j) > sDist) {
					isTrue = false;
					i = mazeConfig.getWidth();
					j = mazeConfig.getHeight();
				}
			}
		}
		assertTrue(isTrue);
	}
	
	/**
	 * Test case: test if the exit is located on the border as 
	 * specified
	 * <p>
	 * Method under test: MazeFactory.order(Order.order), 
	 * MazeConfiguration.getMazeConfig(), mazeConfiguration.getExitPosition()
	 * <p>
	 * Correct behavior: the coordinate of the exit position should
	 * be correct (at least one of the scalers should be 0 or 
	 * width - 1 or height - 1)
	 */
	@Test
	public final void testIfExitOnBorder() {
		assertTrue(mazeFactDtm.order(mockOrder));
		mazeFactDtm.waitTillDelivered();
		MazeConfiguration mazeConfig = mockOrder.getMazeConfig();
		
		int x = mazeConfig.getStartingPosition()[0],
			y = mazeConfig.getStartingPosition()[1];
		boolean isLegal = false;
		if (x == 0 || y == 0)
			isLegal = true;
		else if (x == mazeConfig.getWidth() - 1)
			isLegal = true;
		else if (y == mazeConfig.getHeight() - 1)
			isLegal = true;
		assertTrue(isLegal);
	}

	
//	/**
//	 * Test case: see if the maze has an correct exit by checking if 
//	 * mazeFactDtm.dists has the correct number of elements equal to 1
//	 * <p>
//	 * Method under test: MazeFactory(true), MazeFactory.order(Order order), 
//	 * Distance.getDistanceValues(int x, int y)
//	 * <p>
//	 * Correct behavior: the dists field in mazeFactDtm.builder should have 
//	 * exactly one element that is 1 
//	 */
//	@Test
//	public final void testIfExitExistsAllSkills() {
//		for ( ; skillLevel < 16; ++skillLevel) {
//			System.out.println("skill level is: " + skillLevel);
//			mockOrder = new OrderObject(skillLevel);
//			assertTrue(mazeFactDtm.order(mockOrder));
//			mazeFactDtm.waitTillDelivered();
//
//			MazeConfiguration mazeConfig = mockOrder.getMazeConfig();
//			int exitCount = 0;
//			
//			for (int x = 0; x < mazeConfig.getWidth(); ++x) {
//				for (int y = 0; y < mazeConfig.getHeight(); ++y) {
//					if (mazeConfig.getDistanceToExit(x, y) == 1) {
//						exitCount++;
//					}
//				}
//			}
//			assertEquals(1, exitCount);
//		}
//	}
	
//	/**
//	 * Test case: 
//	 * <p>
//	 * Method under test: 
//	 * <p>
//	 * Correct behavior: 
//	 */
//	@Test
//	public final void testMoban() {
//		
//	}

}
